
package projetomenu;

import java.util.Scanner;

public class ProjetoMenu {


    public static void main(String[] args) {
    
        System.out.println("---------------");
        System.out.println("MENU DE OPÇÕES");
        System.out.println("");
        System.out.println("");
        System.out.println("1 - OPÇÃO 1");
        System.out.println("2 - OPÇÃO 2");
        System.out.println("3 - SAIR");
        System.out.println("");    
        System.out.println("");
        System.out.println("Digite a opção desejada: ");
        System.out.println("---------------");
        
        Scanner teclado = new Scanner(System.in);
        
        int option = teclado.nextInt();
        
        while(option!=3){
        if(option==1){
            System.out.println("Você escolheu a primeira opção.");
            System.out.println("---------------");
            System.out.println("MENU DE OPÇÕES");
            System.out.println("");
            System.out.println("");
            System.out.println("1 - OPÇÃO 1");
            System.out.println("2 - OPÇÃO 2");
            System.out.println("3 - SAIR");
            System.out.println("");    
            System.out.println("");
            System.out.println("Digite a opção desejada: ");
            System.out.println("---------------");
            break;
        }
        if(option==2){
            System.out.println("Você escolheu a segunda opção");
            System.out.println("---------------");
            System.out.println("MENU DE OPÇÕES");
            System.out.println("");
            System.out.println("");
            System.out.println("1 - OPÇÃO 1");
            System.out.println("2 - OPÇÃO 2");
            System.out.println("3 - SAIR");
            System.out.println("");    
            System.out.println("");
            System.out.println("Digite a opção desejada: ");
            System.out.println("---------------");
            break;
        } 
    }
        if(option==3){
            System.out.println("O programa foi encerrado.");
        }
}
}
